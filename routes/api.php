<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\AccountController;
use App\Http\Controllers\CurrencyController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::controller(AuthController::class)->group(function () {
    Route::post('login', 'login');
    Route::post('register', 'register');
    Route::post('logout', 'logout');
    Route::post('refresh', 'refresh');

});

Route::middleware('auth:sanctum')->controller(AccountController::class)->group(function () {
    Route::get('accounts/generateUUID','generateUUID');
    Route::get('accounts/{id}', 'show');
    Route::get('user/accounts','showUserAccount');
    Route::post('accounts/changeStatus','changeStatus');
    Route::post('accounts/{id}', 'update');
    Route::post('accounts', 'store');  
}); 

Route::controller(CurrencyController::class)->group(function () {
    Route::get('currencies', 'index');
}); 