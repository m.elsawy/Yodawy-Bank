import { convertJsonToArray } from "./helper.js";
export default {
    namespaced: true,
    state:{
      accounts: [],
      accountResponse: {
        nextPage: null,
        nextPage: null,
      }
    },
    getters:{
      getAccounts(state)
      {
        return state.accounts;
      },
      getPagesUrl(state)
      {
        return state.accountResponse;
      }
    },
    mutations:{
      setAccountResponse(state,response)
      {
        state.accounts = convertJsonToArray(response.data.resources);
        state.accountResponse.nextPage = response.data.next_page_url;
        state.accountResponse.previousPage = response.data.prev_page_url;
      },
    },
    actions:{
      fetchAccounts(state,data)
      {
        let url = data.url,
            userID = data.userID,
            filter = data.filter,
            filters = "",
            encodeFilter = "";
        if(filter.length == 1)
        {
            encodeFilter += ('[{"class":"App\\\\Nova\\\\Filters\\\\AccountStatusFilter","value":"'+filter[0].value+'"}]');
            filters=btoa(encodeFilter);
        }        
        Nova.request()
          .get(url, {
            params: {
              perPage: 5,
              viaResourceId: userID,
              filters: filters,
              viaResource: "users",
              viaRelationship: "accounts",
              relationshipType: "hasMany",
            },
          })
          .then((response) => {
            this.commit('moduleAccount/setAccountResponse',response);
          });
      },
      changeAccountStatus(state,account)
      {
        Nova.request()
        .post("/api/accounts/changeStatus", { id: account.id })
        .then((response) => {
          account.is_active = response.data.is_active;
        });
      }
    }
  }