export default {
    namespaced: true,
    state:{
        currencies:[],
        currenciesResponse: {
            nextPage: null,
          }
    },
    getters:{
        getCurrencies(state)
        {
            return state.currencies;
        },
        getCurrenciesResponse(state)
        {
            return state.currenciesResponse;
        }
    },
    mutations:{
      updateCurrencies(state,data)
      {
        let loadMore = data.loadMore, response = data.response;
        loadMore
            ? (state.currencies = response.data.resources.concat(state.currencies))
            : (state.currencies = response.data.resources);
        state.currenciesResponse.nextPage = response.data.next_page_url;
      }
    },
    actions:{
        fetchCurrencies(state,data)
        {
            let url = data.url, loadMore = data.loadMore, searchQuery = data.searchQuery; 
            Nova.request()
            .get(url, {
              params: {
                perPage: 3,
                search: searchQuery,
              },
            })
            .then((response) => {
              this.commit('moduleCurrency/updateCurrencies',{response: response,loadMore: loadMore})
            });
        }
    }
}