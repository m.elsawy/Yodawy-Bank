export function convertJsonToArray(jsonObject)
{
    let arrayElements = [];
    jsonObject.forEach(element => {
        let objects = {};
        element.fields.forEach(item =>{
            objects[item.attribute]=item.value;
        })
       
        arrayElements.push(objects);
    });
    return arrayElements;
}
