import CustomTable from "./components/CustomTable.vue";
import PaginationBar from "./components/PaginationBar.vue";
import DropDownIcon from "./components/DropDownIcon.vue";
import CustomSelect from "./components/CustomSelect.vue";
import FormCard from "./components/FormCard.vue";
import FormInput from "./components/FormInput.vue";
import CustomButton from "./components/CustomButton.vue";
import StatusIcon from "./components/StatusIcon.vue";
import moduleAccount from './moduleAccount'
import moduleCurrency from './moduleCurrency'

Nova.booting((Vue, router, store) => {
  Vue.component('nova-bank-resource-tool', require('./components/Tool'))
  
  Vue.component('custom-table',CustomTable);
  Vue.component('pagination-bar',PaginationBar);
  Vue.component('status-icon',StatusIcon);

  Vue.component('drop-down-icon',DropDownIcon);
  Vue.component('custom-select',CustomSelect);
  Vue.component('form-card',FormCard);
  Vue.component('form-input',FormInput);
  Vue.component('custom-button',CustomButton);

  store.registerModule('moduleAccount',moduleAccount);
  store.registerModule('moduleCurrency',moduleCurrency);
})
