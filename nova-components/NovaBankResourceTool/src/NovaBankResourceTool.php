<?php

namespace Yodawy\NovaBankResourceTool;

use Laravel\Nova\ResourceTool;

class NovaBankResourceTool extends ResourceTool
{
    /**
     * Get the displayable name of the resource tool.
     *
     * @return string
     */
    public function name()
    {
        return 'Nova Bank Resource Tool';
    }

    /**
     * Get the component name for the resource tool.
     *
     * @return string
     */
    public function component()
    {
        return 'nova-bank-resource-tool';
    }
}
