/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file.
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier /* server only */
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = injectStyles
  }

  if (hook) {
    var functional = options.functional
    var existing = functional
      ? options.render
      : options.beforeCreate

    if (!functional) {
      // inject component registration as beforeCreate hook
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    } else {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functioal component in vue file
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return existing(h, context)
      }
    }
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(2);
module.exports = __webpack_require__(32);


/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_CustomTable_vue__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_CustomTable_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__components_CustomTable_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_PaginationBar_vue__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_PaginationBar_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__components_PaginationBar_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_DropDownIcon_vue__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_DropDownIcon_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__components_DropDownIcon_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_CustomSelect_vue__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_CustomSelect_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__components_CustomSelect_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_FormCard_vue__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_FormCard_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__components_FormCard_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_FormInput_vue__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_FormInput_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5__components_FormInput_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__components_CustomButton_vue__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__components_CustomButton_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6__components_CustomButton_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__components_StatusIcon_vue__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__components_StatusIcon_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7__components_StatusIcon_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__moduleAccount__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__moduleCurrency__ = __webpack_require__(28);











Nova.booting(function (Vue, router, store) {
  Vue.component('nova-bank-resource-tool', __webpack_require__(29));

  Vue.component('custom-table', __WEBPACK_IMPORTED_MODULE_0__components_CustomTable_vue___default.a);
  Vue.component('pagination-bar', __WEBPACK_IMPORTED_MODULE_1__components_PaginationBar_vue___default.a);
  Vue.component('status-icon', __WEBPACK_IMPORTED_MODULE_7__components_StatusIcon_vue___default.a);

  Vue.component('drop-down-icon', __WEBPACK_IMPORTED_MODULE_2__components_DropDownIcon_vue___default.a);
  Vue.component('custom-select', __WEBPACK_IMPORTED_MODULE_3__components_CustomSelect_vue___default.a);
  Vue.component('form-card', __WEBPACK_IMPORTED_MODULE_4__components_FormCard_vue___default.a);
  Vue.component('form-input', __WEBPACK_IMPORTED_MODULE_5__components_FormInput_vue___default.a);
  Vue.component('custom-button', __WEBPACK_IMPORTED_MODULE_6__components_CustomButton_vue___default.a);

  store.registerModule('moduleAccount', __WEBPACK_IMPORTED_MODULE_8__moduleAccount__["a" /* default */]);
  store.registerModule('moduleCurrency', __WEBPACK_IMPORTED_MODULE_9__moduleCurrency__["a" /* default */]);
});

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(0)
/* script */
var __vue_script__ = __webpack_require__(4)
/* template */
var __vue_template__ = __webpack_require__(5)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/components/CustomTable.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-19878342", Component.options)
  } else {
    hotAPI.reload("data-v-19878342", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 4 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    columns: Array,
    data: Array,
    toggleBtn: Boolean,
    toggleBtnClick: { default: null },
    args: { default: null }
  }
});

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("table", { staticClass: "table-user-accounts" }, [
      _c("thead", [
        _c(
          "tr",
          [
            _vm._l(_vm.columns, function(column) {
              return _c("td", [
                _c("span", [_vm._v(" " + _vm._s(column.name) + " ")])
              ])
            }),
            _vm._v(" "),
            _vm.toggleBtn ? _c("td", [_c("span")]) : _vm._e()
          ],
          2
        )
      ]),
      _vm._v(" "),
      _c(
        "tbody",
        _vm._l(_vm.data, function(element, index) {
          return _c(
            "tr",
            [
              _vm._l(_vm.columns, function(column) {
                return _c(
                  "td",
                  [
                    typeof element[column.attr] == "boolean"
                      ? _c("status-icon", {
                          attrs: { isActive: element["is_active"] }
                        })
                      : _c("span", [
                          _vm._v(
                            "\n              " +
                              _vm._s(element[column.attr]) +
                              "\n            "
                          )
                        ])
                  ],
                  1
                )
              }),
              _vm._v(" "),
              _vm.toggleBtn
                ? _c(
                    "td",
                    [
                      _c("custom-button", {
                        staticClass: "status-btn",
                        class: { activate: !element["is_active"] },
                        attrs: {
                          title: element["is_active"] ? "disable" : "activate",
                          submit: _vm.toggleBtnClick,
                          args: element
                        }
                      })
                    ],
                    1
                  )
                : _vm._e()
            ],
            2
          )
        }),
        0
      )
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-19878342", module.exports)
  }
}

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(0)
/* script */
var __vue_script__ = __webpack_require__(7)
/* template */
var __vue_template__ = __webpack_require__(8)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/components/PaginationBar.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-6acc5584", Component.options)
  } else {
    hotAPI.reload("data-v-6acc5584", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 7 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    nextPage: String,
    previousPage: String,
    getTableData: Function,
    getPageNumber: Function,
    pageNumber: Number
  }
});

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "pagination" }, [
    _c(
      "button",
      {
        class: { disable: !_vm.previousPage },
        on: {
          click: function($event) {
            _vm.getTableData(_vm.previousPage), _vm.getPageNumber("prev")
          }
        }
      },
      [_vm._v("\n      Prev\n    ")]
    ),
    _vm._v(" "),
    _c("span", [_vm._v(_vm._s(_vm.pageNumber))]),
    _vm._v(" "),
    _c(
      "button",
      {
        class: { disable: !_vm.nextPage },
        on: {
          click: function($event) {
            _vm.getTableData(_vm.nextPage), _vm.getPageNumber("next")
          }
        }
      },
      [_vm._v("\n      Next\n    ")]
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-6acc5584", module.exports)
  }
}

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(0)
/* script */
var __vue_script__ = null
/* template */
var __vue_template__ = __webpack_require__(10)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/components/DropDownIcon.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-ec87b196", Component.options)
  } else {
    hotAPI.reload("data-v-ec87b196", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "svg",
    {
      staticClass: "dropdown-icon",
      attrs: {
        xmlns: "http://www.w3.org/2000/svg",
        viewBox: "0 0 24 24",
        width: "24",
        height: "24"
      }
    },
    [
      _c("path", { attrs: { fill: "none", d: "M0 0h24v24H0z" } }),
      _vm._v(" "),
      _c("path", {
        attrs: {
          d: "M12 10.828l-4.95 4.95-1.414-1.414L12 8l6.364 6.364-1.414 1.414z"
        }
      })
    ]
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-ec87b196", module.exports)
  }
}

/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(0)
/* script */
var __vue_script__ = __webpack_require__(12)
/* template */
var __vue_template__ = __webpack_require__(13)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/components/CustomSelect.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-11015938", Component.options)
  } else {
    hotAPI.reload("data-v-11015938", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 12 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    url: String,
    data: Array,
    title: String,
    selectedItem: Array,
    nextPage: String,
    getSearchData: Function,
    getMoreData: Function,
    selectItem: Function,
    searchInput: Boolean
  },
  data: function data() {
    return {
      isVisible: false,
      searchQuery: "",
      numberErrors: null
    };
  },

  methods: {
    getMore: function getMore() {
      this.getMoreData(this.nextPage, true, this.searchQuery);
    },
    updateSearchQuery: function updateSearchQuery(searchQuery) {
      this.searchQuery = searchQuery;
      this.getSearchData(this.url, false, this.searchQuery);
    },
    getErrors: function getErrors(value) {
      this.numberErrors = value;
    }
  }
});

/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("section", { staticClass: "dropdown-wrapper" }, [
    _c(
      "div",
      {
        staticClass: "selected-item",
        on: {
          click: function($event) {
            _vm.isVisible = !_vm.isVisible
          }
        }
      },
      [
        _vm.selectedItem.length == 1
          ? _c("span", [_vm._v(_vm._s(_vm.selectedItem[0].title))])
          : _c("span", [_vm._v("Select " + _vm._s(_vm.title))]),
        _vm._v(" "),
        _c("drop-down-icon")
      ],
      1
    ),
    _vm._v(" "),
    _vm.isVisible
      ? _c(
          "div",
          { staticClass: "dropdown-popover" },
          [
            _vm.searchInput
              ? _c("form-input", {
                  attrs: {
                    updateValue: _vm.updateSearchQuery,
                    type: "text",
                    value: _vm.searchQuery,
                    placeholder: "Search",
                    getErrors: _vm.getErrors
                  }
                })
              : _vm._e(),
            _vm._v(" "),
            _vm.numberErrors
              ? _c("p", { staticClass: "error-msg" }, [
                  _vm._v(_vm._s(_vm.numberErrors))
                ])
              : _vm._e(),
            _vm._v(" "),
            _c("div", { staticClass: "options" }, [
              _c(
                "ul",
                [
                  _vm._l(_vm.data, function(item, index) {
                    return _c(
                      "li",
                      {
                        key: "item-${index}",
                        staticClass: "options",
                        class: {
                          selected: _vm.selectedItem.find(function(element) {
                            return element.title == item.title
                          })
                        },
                        on: {
                          click: function($event) {
                            _vm.selectItem(item), (_vm.isVisible = false)
                          }
                        }
                      },
                      [
                        _vm._v(
                          "\n            " + _vm._s(item.title) + "\n          "
                        )
                      ]
                    )
                  }),
                  _vm._v(" "),
                  _vm.nextPage != null
                    ? _c(
                        "li",
                        [
                          _c("custom-button", {
                            staticClass: "load-more-btn",
                            attrs: { submit: _vm.getMore, title: "load more" }
                          })
                        ],
                        1
                      )
                    : _vm._e()
                ],
                2
              )
            ])
          ],
          1
        )
      : _vm._e()
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-11015938", module.exports)
  }
}

/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(0)
/* script */
var __vue_script__ = __webpack_require__(15)
/* template */
var __vue_template__ = __webpack_require__(16)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/components/FormCard.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-b1561542", Component.options)
  } else {
    hotAPI.reload("data-v-b1561542", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 15 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    title: String
  }
});

/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("h1", { staticClass: "text-90 font-normal text-2xl" }, [
      _vm._v(_vm._s(_vm.title))
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "form-card" }, [_vm._t("default")], 2)
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-b1561542", module.exports)
  }
}

/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(0)
/* script */
var __vue_script__ = __webpack_require__(18)
/* template */
var __vue_template__ = __webpack_require__(19)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/components/FormInput.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-8b7986ea", Component.options)
  } else {
    hotAPI.reload("data-v-8b7986ea", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 18 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      inputValue: ""
    };
  },

  props: {
    type: String,
    updateValue: Function,
    getErrors: Function,
    validateError: String
  },
  methods: {
    isValid: function isValid(event) {
      if (this.type == "number") {
        if (isNaN(this.inputValue)) {
          this.validateError = "Please Enter Valid Number";
        } else {
          this.validateError = "";
        }
      } else if (this.type == "text") {
        var letters = /^[A-Za-z]+$/;
        if (this.inputValue.match(letters) || this.inputValue.length < 1) {
          this.validateError = "";
        } else {
          this.validateError = "Please Enter Valid Text";
        }
      }
    }
  }
});

/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("input", {
    directives: [
      {
        name: "model",
        rawName: "v-model",
        value: _vm.inputValue,
        expression: "inputValue"
      }
    ],
    staticClass: "form-input",
    domProps: { value: _vm.inputValue },
    on: {
      input: [
        function($event) {
          if ($event.target.composing) {
            return
          }
          _vm.inputValue = $event.target.value
        },
        function($event) {
          ;[
            _vm.updateValue($event.target.value),
            _vm.isValid($event.target.value),
            _vm.getErrors(_vm.validateError)
          ]
        }
      ]
    }
  })
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-8b7986ea", module.exports)
  }
}

/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(0)
/* script */
var __vue_script__ = __webpack_require__(21)
/* template */
var __vue_template__ = __webpack_require__(22)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/components/CustomButton.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-edd77f24", Component.options)
  } else {
    hotAPI.reload("data-v-edd77f24", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 21 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    title: String,
    submit: Function,
    args: Object
  },
  methods: {
    handleClick: function handleClick() {
      if (typeof this.submit == 'function') this.submit(this.args);
    }
  }
});

/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "button",
    { staticClass: "btn-item", on: { click: _vm.handleClick } },
    [_c("span", [_vm._v(" " + _vm._s(_vm.title) + " ")])]
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-edd77f24", module.exports)
  }
}

/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(0)
/* script */
var __vue_script__ = __webpack_require__(24)
/* template */
var __vue_template__ = __webpack_require__(25)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/components/StatusIcon.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-e07fb294", Component.options)
  } else {
    hotAPI.reload("data-v-e07fb294", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 24 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    isActive: Boolean
  }
});

/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("span", {
    staticClass: "status-dot",
    class: { active: _vm.isActive }
  })
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-e07fb294", module.exports)
  }
}

/***/ }),
/* 26 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__helper_js__ = __webpack_require__(27);
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


/* harmony default export */ __webpack_exports__["a"] = ({
  namespaced: true,
  state: {
    accounts: [],
    accountResponse: _defineProperty({
      nextPage: null
    }, "nextPage", null)
  },
  getters: {
    getAccounts: function getAccounts(state) {
      return state.accounts;
    },
    getPagesUrl: function getPagesUrl(state) {
      return state.accountResponse;
    }
  },
  mutations: {
    setAccountResponse: function setAccountResponse(state, response) {
      state.accounts = Object(__WEBPACK_IMPORTED_MODULE_0__helper_js__["a" /* convertJsonToArray */])(response.data.resources);
      state.accountResponse.nextPage = response.data.next_page_url;
      state.accountResponse.previousPage = response.data.prev_page_url;
    }
  },
  actions: {
    fetchAccounts: function fetchAccounts(state, data) {
      var _this = this;

      var url = data.url,
          userID = data.userID,
          filter = data.filter,
          filters = "",
          encodeFilter = "";
      if (filter.length == 1) {
        encodeFilter += '[{"class":"App\\\\Nova\\\\Filters\\\\AccountStatusFilter","value":"' + filter[0].value + '"}]';
        filters = btoa(encodeFilter);
      }
      Nova.request().get(url, {
        params: {
          perPage: 5,
          viaResourceId: userID,
          filters: filters,
          viaResource: "users",
          viaRelationship: "accounts",
          relationshipType: "hasMany"
        }
      }).then(function (response) {
        _this.commit('moduleAccount/setAccountResponse', response);
      });
    },
    changeAccountStatus: function changeAccountStatus(state, account) {
      Nova.request().post("/api/accounts/changeStatus", { id: account.id }).then(function (response) {
        account.is_active = response.data.is_active;
      });
    }
  }
});

/***/ }),
/* 27 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = convertJsonToArray;
function convertJsonToArray(jsonObject) {
    var arrayElements = [];
    jsonObject.forEach(function (element) {
        var objects = {};
        element.fields.forEach(function (item) {
            objects[item.attribute] = item.value;
        });

        arrayElements.push(objects);
    });
    return arrayElements;
}

/***/ }),
/* 28 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = ({
    namespaced: true,
    state: {
        currencies: [],
        currenciesResponse: {
            nextPage: null
        }
    },
    getters: {
        getCurrencies: function getCurrencies(state) {
            return state.currencies;
        },
        getCurrenciesResponse: function getCurrenciesResponse(state) {
            return state.currenciesResponse;
        }
    },
    mutations: {
        updateCurrencies: function updateCurrencies(state, data) {
            var loadMore = data.loadMore,
                response = data.response;
            loadMore ? state.currencies = response.data.resources.concat(state.currencies) : state.currencies = response.data.resources;
            state.currenciesResponse.nextPage = response.data.next_page_url;
        }
    },
    actions: {
        fetchCurrencies: function fetchCurrencies(state, data) {
            var _this = this;

            var url = data.url,
                loadMore = data.loadMore,
                searchQuery = data.searchQuery;
            Nova.request().get(url, {
                params: {
                    perPage: 3,
                    search: searchQuery
                }
            }).then(function (response) {
                _this.commit('moduleCurrency/updateCurrencies', { response: response, loadMore: loadMore });
            });
        }
    }
});

/***/ }),
/* 29 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(0)
/* script */
var __vue_script__ = __webpack_require__(30)
/* template */
var __vue_template__ = __webpack_require__(31)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/components/Tool.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-68ff5483", Component.options)
  } else {
    hotAPI.reload("data-v-68ff5483", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 30 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['resourceName', 'resourceId', 'panel'],

  mounted: function mounted() {
    //
  }
});

/***/ }),
/* 31 */
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [_vm._v("Nova Bank Resource Tool")])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-68ff5483", module.exports)
  }
}

/***/ }),
/* 32 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ })
/******/ ]);