<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Currency;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\HasOne;
use Laravel\Nova\Fields\BelongsTo;
use Titasgailius\SearchRelations\SearchesRelations;
use App\Nova\Filters\FromDateFilter;
use App\Nova\Filters\ToDateFilter;
use App\Nova\Filters\AccountStatusFilter;

class Account extends Resource
{
    use SearchesRelations;
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Account::class;

    public static $searchRelations = [
        'user' => ['email'],
    ];

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'account_number';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'account_number',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')
                ->sortable()
                ->rules('required'),
           
            Text::make(__('Account Number'),'account_number')
                ->hideWhenUpdating(),
            
            Number::make(__('Balance'),'balance')
                ->OnlyOnForms()
                ->rules('required'),
            
            Currency::make(__('Balance'),'balance')
                ->onlyOnIndex()
                ->rules('required')->step(0.01)
                ->sortable(),

            Date::make('Created At'),

            Boolean::make('is active','is_active'),

            BelongsTo::make('email', 'user', \App\Nova\User::class)
            ->sortable()
            ->display(function ($user) {
            return $user->email;
            })->hideWhenCreating()
            ->searchable(),

            BelongsTo::make('User')
                ->hideWhenUpdating(),   
            BelongsTo::make('Currency'),
        ];
    }

    public static function searchableRelations(): array
    {
        return [
            'user' => ['email'],
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [
            new FromDateFilter(),
            new ToDateFilter(),
            new AccountStatusFilter(),
        ];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
