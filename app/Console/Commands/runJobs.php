<?php

namespace App\Console\Commands;

use App\Jobs\UpdateAccountStatus;
use App\Models\Account;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Console\Command;

class runJobs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'job:UpdateAccountStatus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run Update Account status Jobs that not have transactions last month';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        UpdateAccountStatus::dispatch();
        return 0;
    }
}
