<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Account;
use App\Models\Currency;
use App\Models\Transaction;
use App\Http\Resources\AccountResource;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Auth;    


class MakeTransaction extends Component
{
    public $userId;

    public $senderAccountId;
    public $senderAccountsItems;
    public $senderAccountsCurrentPage;
    public $senderAccountsNextPage;
    public $senderAccountsIsVisible;
    public $senderAccountSearchQuery;
    public $senderKey;
    
    public $receiverAccountId;
    public $receiverAccounts;
    public $receiverAccountsCurrentPage;
    public $receiverAccountsNextPage;
    public $receiverAccountsIsVisible;
    public $receiverAccountSearchQuery;
    public $receiverKey;

    public $currencyId;
    public $currencies;
    public $currencyCurrentPage;
    public $currencyNextPage;
    public $currencyIsVisible;
    public $currencySearchQuery;
    public $currencyKey;
    private $rates;
    public $currencyRateUrl = 'https://api.exchangerate.host/latest';

    public $balance;

    protected  $listeners = ['senderChange','receiverChange','currencyChange','updateBalance','createClick'
                                ,'getMoreSenderAccounts', 'updateSenderAccountSearchQuery','toggleVisibleSenderAccount'
                                ,'getMoreReceiverAccounts', 'updateReceiverAccountSearchQuery','toggleVisibleReceiverAccount'
                                ,'getMoreCurrency', 'updateCurrencySearchQuery','toggleVisibleCurrency'
                            ];
    protected $rules = [
            'senderAccountId' => 'required|exists:accounts,account_number',
            'receiverAccountId' => 'required|exists:accounts,account_number',
            'balance' => 'required|numeric|min:1',
            'currencyId' => 'required|exists:currencies,id'
    ];
    protected $messages = [

        'senderAccountId.required' => "Please select sender account",
        'receiverAccountId.required' => "Please select receiver account",
        'currencyId.required' => "Please select currency",
    ];

    /**
     * Calculate balance by given currency ID from current amount
     * @param int $amount the amount of balance 
     * @param int $currencyId to get rate of currency 
     * @return int balance after calculate currency rate 
    */
    public function calculateBalance($amount,$currencyId)
    {
        $name = Currency::find($currencyId)->name;
        return $amount / $this->rates[$name];
    }
     /**
     * Convert balance to current amount  by given currency ID  
     * @param int $amount 
     * @param int $currencyId to get rate of currency 
     * @return int amount after convert from currency rate 
    */
    public function convertBalance($amount,$currencyId)
    {
        $name = Currency::find($currencyId)->name;
        return $amount * $this->rates[$name];
    }
    /**
     * get and set sender account select items
    */
    public function setSenderAccountsListItems($searchQuery)
    {
        //get data and convert to key value object
        $senderAccounts = Account::where('account_number','LIKE', '%'.$searchQuery.'%')
                                 ->where('user_id','=',$this->userId)
                                 ->paginate(3, ['*'], 'page',1);
        
        $this->senderAccountsItems = $senderAccounts->pluck('account_number','id');
       
        //set sender select box variables 
        $this->senderAccountSearchQuery = $searchQuery ;
        $this->senderAccountId = null ;
        $this->senderAccountsCurrentPage  =  $senderAccounts->currentPage();
        $this->senderAccountsNextPage =  $senderAccounts->nextPageUrl();
    }
    /**
     * get and set receiver account select items
    */
    public function setReceiverAccountsListItems($searchQuery)
    {
        //get data and convert to key value object
        $receiverAccounts = Account::where('id','<>',$this->senderAccountId)
                                    ->where('account_number','LIKE', '%'.$searchQuery.'%')
                                    ->where('user_id','=',$this->userId)
                                    ->paginate(3, ['*'], 'page',1);
        
        $this->receiverAccounts = $receiverAccounts->pluck('account_number','id');
        
        //set receiver select box variables 
        $this->receiverAccountSearchQuery = $searchQuery ;
        $this->receiverAccountsCurrentPage  = $receiverAccounts->currentPage();
        $this->receiverAccountsNextPage = $receiverAccounts->nextPageUrl();
        $this->receiverAccountId = null ;
    }
    /**
     * get and set receiver account select items
    */
    public function setCurrencyListItems($searchQuery)
    {
        //get data and convert to key value object
        $currencies = Currency::where('name','LIKE', '%'.$searchQuery.'%')
                                ->paginate(3, ['*'], 'page',1);
        
        $this->currencies = $currencies->pluck('name','id');

        //set currency select box variables 
        $this-> currencySearchQuery = $searchQuery ;
        $this->currencyId = null ;
        $this->currencyCurrentPage  = $currencies->currentPage();
        $this->currencyNextPage = $currencies->nextPageUrl();
    }
    public function toggleVisibleSenderAccount($isVisible)
    {
        $this->senderAccountsIsVisible = !$isVisible;
        //close other select box 
        if(!$isVisible)
        {
            $this->receiverAccountsIsVisible = false;
            $this->currencyIsVisible = false;
        }
    }
    public function toggleVisibleReceiverAccount($isVisible)
    {
        $this->receiverAccountsIsVisible = !$isVisible;
        //close other select box 
        if(!$isVisible)
        {
            $this->senderAccountsIsVisible = false;
            $this->currencyIsVisible = false;
        }
    }
    public function toggleVisibleCurrency($isVisible)
    {
        $this->currencyIsVisible = !$isVisible;
        //close other select box 
        if(!$isVisible)
        {
            $this->receiverAccountsIsVisible = false;
            $this->senderAccountsIsVisible = false;
        }
    }
    public function mount()
    {
        $this->getRates();

        $this->userId = Auth::user()->id ;

        $this->setSenderAccountsListItems("");

        $this->setReceiverAccountsListItems("");

        $this->setCurrencyListItems("");        
    }
    /**
     * trigger sender select box change action
    */
    public function senderChange($selectedItem)
    {
        $this->senderAccountId = $selectedItem;
        $this->senderAccountsIsVisible = false ;
        
        //check if select same account number 
        if($this->senderAccountId == $this->receiverAccountId && $this->senderAccountId != null)
        {
            $this->setReceiverAccountsListItems("");
        }
    }
    /**
     * trigger receiver select box change action
    */
    public function receiverChange($selectedItem)
    {
        $this->receiverAccountId = $selectedItem;
        $this->receiverAccountsIsVisible = false ;

        //check if select same account number 
        if($this->senderAccountId == $this->receiverAccountId && $this->senderAccountId != null)
        {
            $this->setSenderAccountsListItems("");
        }
    }
    /**
     * trigger currency select box change action
    */
    public function currencyChange($selectedItem)
    {
        $this->currencyId = $selectedItem;
        $this->currencyIsVisible = false ;
    }
    public function updateBalance($inputValue)
    {
        $this->balance = $inputValue;
    }
    public function createClick()
    {   
        $this->submit();
    }
    public function getMoreSenderAccounts($searchQuery)
    {  
        $senderAccounts = Account::where('account_number','LIKE', '%'.$searchQuery.'%')
                                 ->where('user_id','=',$this->userId)
                                 ->paginate(3, ['*'], 'page', $this->senderAccountsCurrentPage + 1);
        
        $this->senderAccountsItems = $this->senderAccountsItems
                                          ->union($senderAccounts->pluck('account_number','id'));

        $this->senderAccountsCurrentPage =  $senderAccounts->currentPage();
        $this->senderAccountsNextPage =  $senderAccounts->nextPageUrl();
        $this->senderKey = md5(json_encode([    $this->senderAccountId , $this->senderAccountsItems,
                                                $this->senderAccountsCurrentPage, $this->senderAccountsNextPage,
                                                $this->senderAccountSearchQuery 
                                ]));
    }
    public function getMoreReceiverAccounts($searchQuery)
    {   
        $receiverAccounts = Account::where('account_number','LIKE', '%'.$searchQuery.'%')
                                    ->where('user_id','=',$this->userId)
                                    ->paginate(3, ['*'], 'page', $this->receiverAccountsCurrentPage + 1);
        
        $this->receiverAccounts = $this->receiverAccounts
                                       ->union($receiverAccounts->pluck('account_number','id'));
        
        $this->receiverAccountsCurrentPage = $receiverAccounts->currentPage();
        $this->receiverAccountsNextPage = $receiverAccounts->nextPageUrl();
    }
    public function getMoreCurrency($searchQuery)
    { 
        $currencies = Currency::where('name','like','%'.$searchQuery.'%')
                                ->paginate(3, ['*'], 'page', $this->currencyCurrentPage + 1);
        
        $this->currencies = $this->currencies
                                 ->union($currencies->pluck('name','id'));

        $this->currencyCurrentPage = $currencies->currentPage();
        $this->currencyNextPage = $currencies->nextPageUrl();

    }
    public function updateSenderAccountSearchQuery($searchQuery)
    {
        $this->setSenderAccountsListItems($searchQuery);    
    }
    public function updateReceiverAccountSearchQuery($searchQuery)
    {
        $this->setReceiverAccountsListItems($searchQuery);    
    }
    public function updateCurrencySearchQuery($searchQuery)
    {
        $this->setCurrencyListItems($searchQuery);    
    }
    /**
     * submit form data
    */
    public function submit()
    {    
        //validate form required data 
        $this->validate();

        /* 
        * get sender and receiver current balance 
        * calculate sender balance by selected currency 
        */  
        $senderAccount = Account::find($this->senderAccountId);
        $receiverAccount = Account::find($this->receiverAccountId);

        $senderAccountBalance = $this->calculateBalance($senderAccount->balance, $senderAccount->currency_id);
        $senderBalance = $this->convertBalance($senderAccountBalance, $this->currencyId);
        
        
        //validate available balance 
        $validatedData = $this->validate([
            'balance' => 'numeric|min:1|max:'.$senderBalance,
        ],
        [ 
            'balance.max' => 'no available balance'
        ]
        );  

        //get current input balance 
        $currentBalance = $this->calculateBalance($this->balance, $this->currencyId);
        
        Transaction::create([
            'send_account_id' =>  $this->senderAccountId,
            'receive_account_id' => $this->receiverAccountId,
            'currency_id' => $this->currencyId,
            'amount' => $this->balance
        ]);
        

        //get transfer amount using currency of account to remove from account balance  
        $senderBalanceTransfer = $this->convertBalance($currentBalance, $senderAccount->currency_id,); 
        Account::where('id','=',$this->senderAccountId)
               ->update([
                            'balance' => ($senderAccount->balance  - $senderBalanceTransfer) ,
                        ]);       
        
        //get transfer amount using currency of account to add to account balance  
        $receiverBalanceTransfer = $this->convertBalance($currentBalance, $receiverAccount->currency_id); 
        Account::where('id','=',$this->receiverAccountId)
                ->update([
                            'balance' => $receiverAccount->balance + $receiverBalanceTransfer ,
                        ]);
    }
    public function getRates()
    {
        $response_json = Http::get($this->currencyRateUrl);
        $response = $response_json->json($key=null);
        if(false !== $response_json) {
            if($response_json->successful() === true) {
                $this->rates = $response['rates'];
            }
        }
    }
    public function render()
    {
        return view('livewire.make-transaction');
    }
}
