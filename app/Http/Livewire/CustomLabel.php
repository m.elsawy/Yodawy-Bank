<?php

namespace App\Http\Livewire;

use Livewire\Component;

class CustomLabel extends Component
{
    public $text;
    public $cssClass;
    public function mount($text,$cssClass)
    {
        $this->text = $text ;
        $this->cssClass = $cssClass;
    }
    public function render()
    {
        return view('livewire.custom-label');
    }
}
