<?php

namespace App\Http\Livewire;

use Livewire\Component;

class CustomInput extends Component
{
    public $textModel;
    public $type;
    public $updatedText;
    public $cssClass;
    public function mount()
    {
      
    }
    public function updateTextValue()
    {
        $this->emitUp($this->updatedText, $this->textModel);
    }
    public function render()
    {
        return view('livewire.custom-input');
    }
}
