<?php

namespace App\Http\Livewire;

use Livewire\Component;

class CustomButton extends Component
{
    public $text;
    public $type;
    public $selectedBtn;
    public $cssClass;

    public function mount($text, $type, $selectedBtn,$cssClass)
    {
        $this->text = $text;
        $this->type = $type;
        $this->selectedBtn = $selectedBtn;
        $this->$cssClass = $cssClass;
    }
    public function onClick()
    {
        $this->emitUp($this->selectedBtn);
    }
    public function render()
    {
        return view('livewire.custom-button');
    }
}
