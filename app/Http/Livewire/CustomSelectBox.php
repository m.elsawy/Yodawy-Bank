<?php

namespace App\Http\Livewire;

use Livewire\Component;

class CustomSelectBox extends Component
{
    public $selectedItem;
    public $items;
    public $selectChange;
    public $toggleVisibleItem;
    public $cssClass;
    public $getMoreBtn;
    public $hasLoadMore;
    public $searchQuery;
    public $updateSearchQuery;
    public $openedBox;

    public $isVisible ;
    public $title;

    protected  $listeners = ['loadMore','updateSearchQuery','closeSelectBox'];

    public function mount()
    {
        
    }
    public function toggleVisible()
    {
        $this->emitUp($this->toggleVisibleItem, $this->isVisible);
    }
    public function selectChange($key)
    {
        $this->emitUp($this->selectChange, $key);
        $this->toggleVisible();
    }
    public function loadMore()
    {   
        $this->emitUp($this->getMoreBtn,$this->searchQuery);
    }
    public function updateSearchQuery($searchQuery)
    {   
        $this->emitUp($this->updateSearchQuery, $searchQuery);
    }
    public function render()
    {
        return view('livewire.custom-select-box');
    }
}
