<?php

namespace App\Http\Controllers;
use App\Models\Account;
use App\Models\Currency;
use App\Http\Resources\AccountResource;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use Laravel\Nova\Http\Middleware\Authenticate;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;    
use Laravel\Nova;
use Acme\CreateAccount\Http\Middleware\Authorize;
use Bouncer;

class AccountController extends Controller
{   
    
    public function __construct()
    {
        
    }

    /**
     * Get a account info using accountID.
     * 
     * @param  int $id
     * 
     * @return App\Http\Resources\AccountResource
     */
    public function show($id)
    {       
        $account=Account::find($id);

        $this->authorize('view', $account);
        
        return new AccountResource($account);
    }

    /**
     * Get a list users account info using UserID.
     * 
     * @param  int $user_id
     * 
     * @return App\Http\Resources\AccountResource
     */
    public function showUserAccount(Request $request)
    {
        (!$request['per-page'])? $page = 10 : $page = $request['per-page'];
        $user_id = Auth::user()->id;
        $account=Account::where('user_id','=',$user_id)->paginate($page);
        return AccountResource::collection($account);
    }

    /**
     * Create new account.
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $myRequestObject)
    {
        $rules=array(
            'currency_name' => 'exists:currencies,name',
            'user_id' => 'exists:users,id',
            'balance' => 'required'
        );
        $validator = Validator::make($myRequestObject->all(), $rules);
        if ($validator->fails()) {
            $code=$this->returnCodeAccordingToInput($validator);
               return  $this->returnValidationError($code,$validator);
        } 
        else
        {
            $account = $myRequestObject->all();
            $currency_id = Currency::select('id')->where('name','=',$account['currency_name'])->get()->first();
            $account=Account::create([
                'account_number' => Str::uuid(),
                'currency_id' => $currency_id['id'],
                'user_id' => $account['user_id'],
                'balance' => $account['balance']
            ]);

            return response()->json([
                'status' => true,
                'message' => "Account Created successfully!",
                'account' => $account
            ], 200);
        }
    }
    
    /**
     * Update account info using accountID.
     * 
     * @param  int $id
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request,$id)
    {
        $rules=array(
            'currency_name' => 'exists:currencies,name',
            'user_id' => 'exists:users,id',
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
           return  $validator->messages();
        } 
        else
        {
            $account = Account::find($id);
            if($account==null)
            {
                return ['User Not Found'];
            }
            Gate::authorize('update',$account);
            $currency_id = Currency::select('id')->where('name','=',$request['currency_name'])->get()->first();
            $account->update([
                'currency_id' => $currency_id['id'],
                'user_id' => $request['user_id'],
                'balance' => $request['balance']
            ]);
            
            return response()->json([
                'status' => true,
                'message' => "Account Updated successfully!"
            ], 200);
        }
    }
    /**
     * Toggle status of account.
     * 
     * @param  int $id
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeStatus(Request $request)
    {
        $account = Account::find($request['id']);
        if(is_null($account))
        {
            return response()->json([
                'status' => false,
                'message' =>'Account not found'
            ],400);
        }
        $account->update([
            'is_active' => (!$account['is_active']),
        ]);
        return response()->json([
            'is_active' => $account['is_active'],
            'status' => true,
            'message' => "Status Change Successfully"
        ],200);
    }
    /**
     * Generate UUID to account number
     * 
     * @return string 
     */
    public function generateUUID()
    {
        return Str::uuid();
    }

}
