<?php

namespace App\Jobs;

use Carbon\Carbon;
use App\Models\Account;
use App\Models\Transaction;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

  
class UpdateAccountStatus implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
    
    }
 
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $monthDate=Carbon::now()->subMonth()->toDateTimeString();
        $nowDate=Carbon::now()->toDateTimeString();
        $updatemethod = 
        Account::whereHas('SendAccountRelations',function($q) use ($monthDate,$nowDate){
            $q->whereNotBetween('created_at',[$monthDate,$nowDate]);
        })
        ->WhereHas('ReceiveAccountRelations',function($q) use ($monthDate,$nowDate) {
            $q->whereNotBetween('created_at',[$monthDate,$nowDate]);
        })
        ->orWhere(function($q){
            $q->doesntHave('SendAccountRelations')
                ->doesntHave('ReceiveAccountRelations');
        })
        ->update([
            'isActive' => false,
        ]);
        dump($updatemethod);
    }
    
}
