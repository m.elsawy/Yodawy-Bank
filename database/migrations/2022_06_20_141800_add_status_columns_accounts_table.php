<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStatusColumnsAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('accounts', 'isActive'))
        {
            Schema::table('accounts', function (Blueprint $table) {
                $table->boolean('isActive')->default(0);
            });
        } 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('accounts', 'isActive'))
        {
            Schema::table('accounts',function(Blueprint $table){
                $table->dropColumn('isActive');
            });
        }
    }
}
