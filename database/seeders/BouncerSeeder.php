<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Account;
use App\Models\User;
use Bouncer;

class BouncerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::find(3);
        Bouncer::allow($user)->to('view-account', Account::class);
        Bouncer::allow($user)->to('edit-account', Account::class);
    }
}
