<section class="dropdown-wrapper">
    <div wire:click="toggleVisible" class="selected-item">
    @if($selectedItem > 0 )  
    <span>{{ $items[$selectedItem] }}</span>
    @else
      <span v-else>Select {{ $title }}</span>
    @endif
    </div>
    @if($isVisible)
        <div class="dropdown-popover">  
            <livewire:custom-input 
            :updatedText="'updateSearchQuery'"
            :type="'text'"
            :textModel="$searchQuery"
            :wire:key="'custom-input'.now()"
            :cssClass="'form-input'"
            />
            <div class="options">
                <ul>
                @foreach($items as $key => $value)
                    <li
                        class="{{($key == $selectedItem) ? 'options selected' : 'options'}}"
                        wire:click="selectChange({{$key}})"
                        wire:key="item-{{ $key }}"
                    >
                        {{$value}}
                    </li>
                @endforeach
                    @if($hasLoadMore)
                        <li v-if="nextPage != null">
                            <livewire:custom-button
                            :type="'button'"
                            :text="'load more'"
                            :selectedBtn="'loadMore'"
                            :cssClass="'load-more-btn'"
                            wire:key="'load-more'"
                            />
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    @endif
  </section>