<button type="{{$type}}" class="{{$cssClass}}" wire:click="onClick">
    {{$text}}
</button>
