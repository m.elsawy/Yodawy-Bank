<form wire:submit.prevent="submit" class="create-form">
    <div class="card">
        <livewire:custom-label
            :text="'Send Account'"
            :cssClass="'form-label'"
        />
        <div class="input-box">
            <livewire:custom-select-box
            :selectedItem="$senderAccountId"
            :items="$senderAccountsItems"
            :selectChange="'senderChange'"
            :getMoreBtn="'getMoreSenderAccounts'"
            :hasLoadMore="$senderAccountsNextPage"
            :cssClass="'form-select'"
            :title="'Sender Account'"
            :searchQuery="$senderAccountSearchQuery"
            :updateSearchQuery="'updateSenderAccountSearchQuery'"
            :toggleVisibleItem="'toggleVisibleSenderAccount'"
            :wire:key="'sender-select-'.now()"
            :isVisible="$senderAccountsIsVisible"
            />
            @error('senderAccountId') <span class="validation">{{ $message }}</span> @enderror
        </div>
    </div>
    <div class="card">
        <livewire:custom-label
            :text="'Receive Account'"
            :cssClass="'form-label'"
        />
        <div class="input-box">
            <livewire:custom-select-box
            :selectedItem="$receiverAccountId"
            :items="$receiverAccounts"
            :selectChange="'receiverChange'"
            :getMoreBtn="'getMoreReceiverAccounts'"
            :hasLoadMore="$receiverAccountsNextPage"
            :cssClass="'form-select'"
            :title="'receiver Account'"
            :searchQuery="$receiverAccountSearchQuery"
            :updateSearchQuery="'updateReceiverAccountSearchQuery'"
            :toggleVisibleItem="'toggleVisibleReceiverAccount'"
            :wire:key="'receiver-select-'.now()"
            :isVisible="$receiverAccountsIsVisible"
            />
            @error('receiverAccountId') <span class="validation">{{ $message }}</span> @enderror
        </div>
    </div>
    <div class="card">
        <livewire:custom-label
            :text="'currency'"
            :cssClass="'form-label'"
        />
        <div class="input-box">
            <livewire:custom-select-box
            :selectedItem="$currencyId"
            :items="$currencies"
            :selectChange="'currencyChange'"
            :getMoreBtn="'getMoreCurrency'"
            :hasLoadMore="$currencyNextPage"
            :cssClass="'form-select'"
            :title="'currency'"
            :searchQuery="$currencySearchQuery"
            :updateSearchQuery="'updateCurrencySearchQuery'"
            :toggleVisibleItem="'toggleVisibleCurrency'"
            :wire:key="'currency-select-'.now()"
            :isVisible="$currencyIsVisible"
            />
            @error('currencyId') <span class="validation">{{ $message }}</span> @enderror
        </div>
    </div>
    <div class="card">
        <livewire:custom-label
            :text="'Balance'"
            :cssClass="'form-label'"
        />
        <div class="input-box">
            <livewire:custom-input 
                :textModel="$balance"
                :type="'number'"
                :updatedText="'updateBalance'"
                :cssClass="'form-input'"
                :wire:key="'custom-input-'.$balance"
            />
            @error('balance') <span class="validation">{{ $message }}</span> @enderror
        </div>
    </div>
        <livewire:custom-button
            :type="'button'"
            :text="'Create'"
            :selectedBtn="'createClick'"
            :cssClass="'form-submit-btn'"
            :wire:key="'custom-button-submit'"
        />
</form>