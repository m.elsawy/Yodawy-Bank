<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Create Transaction</title>
        <livewire:styles />
        <link rel="stylesheet" href="{{ asset('css/createTransactionsStyle.css') }}">
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    </head>
    <body>
        <div class="container">
            <label class="title">Create Transaction</label>
            <livewire:make-transaction />
        </div>
        <livewire:scripts />
    </body>
</html>